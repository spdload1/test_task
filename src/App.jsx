import React from 'react'
import { SearchPage } from './pages'

import './App.css'

function App() {
  return (
    <div className="App">
      <SearchPage />
    </div>
  )
}

export default App
