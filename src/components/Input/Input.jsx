import React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types'

function Input({ className, ...rest }) {
  return (
    <input
      className={classnames('Input', className)}
      {...rest}
    />
  )
}

Input.propTypes = {
  intent: PropTypes.string,
  className: PropTypes.string
}

Input.defaultProps = {
  intent: '',
  className: ''
}
export default Input
