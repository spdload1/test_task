import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { childrenType } from '../../propTypes'

function List({ children, customClasses }) {
  return (
    <ul className={classnames(customClasses)}>
      {children}
    </ul>
  )
}
List.defaultProps = {
  customClasses: ''
}

List.propTypes = {
  children: childrenType.isRequired,
  customClasses: PropTypes.string
}

export default List
