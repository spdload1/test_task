import React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types'
import { childrenType } from '../../propTypes'

function ListItem({ children, type, customClasses }) {
  return (
    <li className={classnames(customClasses)} type={type}>
      {children}
    </li>
  )
}

ListItem.defaultProps = {
  customClasses: '',
  type: 'none'
}

ListItem.propTypes = {
  children: childrenType.isRequired,
  customClasses: PropTypes.string,
  type: PropTypes.oneOf(['none', 'decimal', 'disc'])
}

export default ListItem
