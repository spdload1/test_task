/* eslint-disable import/prefer-default-export */
export function isMatchSubstring ({ item, subString }) {
    return !!Object.values(item).join(' | ').trim().toLowerCase().match(subString.toLowerCase())
  }

export function filterItemsBySubString({ arrayOfItems, subString }) {
  if(!subString) {
    return arrayOfItems
  }
  return arrayOfItems.filter((item) => isMatchSubstring({ item, subString: subString.trim() }))
}

export function filterItemsBySubStrings({ arrayOfItems, subStrings }) {
  return arrayOfItems.filter(item => {
    const result = subStrings.filter(subString => {
      return isMatchSubstring({ item, subString: subString.trim() })
    })
    return !!result.length
  })
}

export function findWithANDOperator ({ arrayOfItems, subStrings }) {
  const newSubstrings = [...subStrings]
  const filteredArray = filterItemsBySubString({ arrayOfItems, subString: newSubstrings.shift() })
  if (filteredArray.length && newSubstrings.length) {
    return findWithANDOperator({arrayOfItems: filteredArray, subStrings: newSubstrings })
  }
  return filteredArray
}

export function findWithOROperator ({ arrayOfItems, subStrings }) {
  return filterItemsBySubStrings({ arrayOfItems, subStrings })
}