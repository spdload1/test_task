import React, { useState } from 'react'
import { Formik } from 'formik' 
import { useMount} from 'react-use'
import * as yup from 'yup'
import './SearchPage.css'

import { Input, List, ListItem } from '../../components'
import { filterItemsBySubString, findWithANDOperator, findWithOROperator } from '../../helpers/searchHelpers' 

function SearchPage() {
  const [ team, setTeam ] = useState([])

  useMount(async function getTeam() {
    const { default: developers } = await (import('../../mocks/developers.json'))
    setTeam(developers)
  })
  return (
    <div className="search-page">
      <h1>Start typing</h1>
      <Formik
        initialValues={{ searchField: '' }}
        validationSchema={yup.object().shape({ searchField:
          yup.string().test('operators', 'You should use operator only once!', (value) => {
            return value.split(/OR|AND/).length < 3
        })})}
        render={({ values, handleChange, errors }) => {
          let findedMembers = []
          const withOROperator = values.searchField.split(/OR/)
          const withANDOperator = values.searchField.split(/AND/)
          if (withANDOperator.length > 1) {
            findedMembers = findWithANDOperator({ 
              arrayOfItems: team,
              subStrings: withANDOperator,
            })
          }
          else if (withOROperator.length > 1) {
            findedMembers = findWithOROperator({ 
              arrayOfItems: team,
              subStrings: withOROperator,
            })
          }
          else {
            findedMembers = filterItemsBySubString({arrayOfItems: team, subString: values.searchField})
          }

          return (
            <div className="search-block">
              <div className="search-input-wrapper">
                <Input
                  type="text"
                  onChange={handleChange}
                  value={values.searchField}
                  name="searchField"
                  className="search-input"
                />
                <div className="error-message">{errors.searchField}</div>
              </div>
              <div className="list-block">
                <List>
                  {!errors.searchField && findedMembers.map((member) => (
                    <ListItem type="disc" key={member.name}>
                    Сотрудник: {member.name}, должность: {member.position}, технология: {member.technology}
                    </ListItem>
))}
                </List>
              </div>
            </div>
)
        }}
      />
    </div>
)
}

export default SearchPage